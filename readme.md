# Playlist Express CRUD - SQL 
## TS based express rest API

It implements a users, playlists, songs and artists entities and routers, and handle incoming requests to **Create**, **Update** and **Delete** each one of them by authorization.
It also allow us to **Read** a single entity and return a **list** of all existing ones, with an option of **pagination**.
Our DB is  **SQL**.

It also implements a **log plain text file** that logs each http request made to the server in a single line and includes the request http **method**, the **path** and a **timestamp**.
In addition, it implements an **Error log plain text file** that logs each error includes the **error status**, the **error message**, the **the requestID**, a **timestamp** and the **error stack**.