import { NextFunction, Request, Response } from "express";

export interface IArtist {
    id: number;
    first_name: string;
    last_name: string;
    nickname?: string;
    email?: string;
    about?: string;
    status: string;
    // songs?: number[];
}

export interface ISong {
    id: number;
    name: string;
    time: string;
    artist_id: number;
    lyrics?: string;
    status: string;
    // playlists?: number[];
    // writers?: string[];
    // label?: string;
}
export interface IPlaylist {
    id: number;
    name: string;
    user_id: number;
    // number_of_songs?: number;
    // total_time?: string;
    // songs?: number[];
}
export interface IUser {
    id: number;
    first_name: string;
    last_name: string;
    email: string;
    password?: string;
    refresh_token?: string;
    roles: string;
    // username: string;
    // phone: string;
}

export enum user_roles {
    Admin = "admin",
    Moderator = "moderator",
    Simple = "simple",
    Guest = "guest",
}

export enum entity_status {
    Accepted = "accepted",
    Pending = "pending",
    Simple = "rejected",
}
export interface IResponseMessage {
    status: number;
    message: string;
    data: any;
}
export interface IErrorResponse {
    status: number;
    message: string;
    stack?: string;
}

export type middleWareFunction = (
    req: Request,
    res: Response,
    next: NextFunction
) => any;
