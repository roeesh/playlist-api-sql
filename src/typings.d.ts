export declare interface IArtist{
  id: string;
    first_name: string;
    last_name: string;
    nickname?: string;
    email?: string;
    songs?: ISongs[];
    about?: string;
}

declare global {

  namespace Express {
    interface Request {
        requestID: string;
        userID: string;
        userRoles: string;
    }
  }
}
