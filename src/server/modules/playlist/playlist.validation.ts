import Joi from "joi";

export const schemaForCreatePlaylist = Joi.object({
    name: Joi.string().min(2).max(30).required(),
});

export const schemaForUpdatePlaylist = Joi.object({
    name: Joi.string().min(2).max(30),
});
