import { connection as db} from "../../db/mysql.connection.js";
import { IPlaylist } from "../../../types/types.js";
// import playlist_model from "../playlist/playlist.model.js";
// import song_model from "../song/song.model.js";
// // import pkg from "bluebird";
// import { deletePlaylistFromSong } from "../song/song.service.js";
// import user_model from "../user/user.model.js";
// import { deletePlaylistFromUser } from "../user/user.service.js";
import HttpException from "../../exceptions/http-exception.js";
import { RowDataPacket } from "mysql2";

// const { Promise } = pkg;
export const SONG_EXISTS = "song already exists in this playlist";

// Create new playlist
export async function createNewPlaylist(
    reqBody: IPlaylist,
    creatorID: number
): Promise<any> {
    const playlistInfo: IPlaylist = {...reqBody, user_id: creatorID};
    
    const sql = "INSERT INTO playlists SET ?";
    const [results] = await db.query(sql,playlistInfo);
    const json: any = results;
    
    const newPlaylist = await getPlaylistByID(Number(json.insertId));
    return newPlaylist;
}

// Get all playlists
export async function getAllplaylists(): Promise<any> {
    const [playlists] = await db.execute("SELECT * FROM playlists;");
    // console.log({playlists});
    return playlists;
}

// Add a song to playlist (and update song.playlists field and playlist.total_time & playlist.number_of_songs fields)
export const addSongToPlaylist = async (playlistID: number, songID: number, userID: number) => {
    const [playlistToUpdate] = await getPlaylistByID(playlistID);
    if(!playlistToUpdate){
        return undefined;
    }
    // Check If Song Exists In Playlist
    const checkIfSongInPlaylistSql = `SELECT * FROM playlist_song 
                                      WHERE playlist_id = ${playlistID} AND song_id = ${songID};`;
    const [results] = await db.execute(checkIfSongInPlaylistSql) as RowDataPacket[];
    if(results.length !== 0){
        return SONG_EXISTS;
    }

    // Only playlist creator can update his playlist
    if(playlistToUpdate.user_id != userID) {
        throw new HttpException(
            403,
            "You are not allowed to update this playlist."
        );
    }
    const sql = "INSERT INTO playlist_song (playlist_id,song_id) VALUES (?,?);";
    const newRow = [playlistID,songID];
    await db.query(sql,newRow);
    const joinSql = `SELECT playlists.*, GROUP_CONCAT(distinct songs.name) as songs
                     FROM playlists JOIN playlist_song ON playlists.id = playlist_song.playlist_id
                        JOIN songs ON songs.id = playlist_song.song_id
                    WHERE playlists.id = ${playlistID}
                    GROUP BY playlists.id`;
    const [playlistWithSongs] = await db.execute(joinSql);
    return playlistWithSongs;
};

// Get a single playlist
export async function getPlaylistByID(idToRead: number): Promise<any> {
    const sql = `SELECT playlists.*, GROUP_CONCAT(distinct songs.name) as songs
                 FROM playlists LEFT JOIN playlist_song ON playlists.id = playlist_song.playlist_id
                    LEFT JOIN songs ON songs.id = playlist_song.song_id
                 WHERE playlists.id = ${idToRead}
                 GROUP BY playlists.id`;
    const [playlistWithSongs] = await db.execute(sql) as RowDataPacket[];
    if(playlistWithSongs.length === 0){
        return undefined;
    }
    return playlistWithSongs;
}

// Update a playlist using the id
export async function updatePlaylist(
    idToUpdate: number,
    reqBody: IPlaylist,
    userID: number
): Promise<any> {
    const [playlistToUpdate] = await getPlaylistByID(idToUpdate);
    console.log("playlistToUpdate: ",playlistToUpdate);
    // Only playlist creator can update his playlist
    if(playlistToUpdate.user_id != userID) {
        throw new HttpException(
            403,
            "You are not allowed to update this playlist."
        );
    }

    const playlistInfo = reqBody;
    const sql = `UPDATE playlists SET ? WHERE id= ${idToUpdate};`;
    await db.query(sql,playlistInfo);

    const updatedPlaylist = await getPlaylistByID(idToUpdate);
    return updatedPlaylist;
}

// Delete a playlist using the id (and delete it also from each song)
// Using function overload to support delete requests from user (checks if the userID = playlist.created_by)
export async function deletePlaylistByID(idToDelete: number): Promise<any>;
export async function deletePlaylistByID(idToDelete: number, userID: number): Promise<any>;
export async function deletePlaylistByID(arg1: number, arg2?: number): Promise<any> {
    if(arg2 !== undefined){ //got userID from delete request by user
        const [playlistToCheck] = await getPlaylistByID(arg1);
        // Only playlist creator can delete his playlist
        if(playlistToCheck.user_id != arg2) {
            throw new HttpException(
                403,
                "You are not allowed to delete this playlist."
            );
        }
    }
    const sql = `DELETE FROM playlists WHERE id = ${arg1};`;
    const deletedPlaylist = await getPlaylistByID(arg1);
    await db.query(sql);
    
    return deletedPlaylist;
}

// **** helper functions for song service ****

// update song in each playlist in which the song in (Playlist.songs)
// export async function updatePlaylistOfSongByPlaylistID(
//     playlistID: number,
//     songIDToUpdate: number,
//     song: ISong,
//     oldSong: ISong
// ) {
//     const playlistOfSong = await playlist_model.findById(playlistID);

//     // update playlist total time
//     const totalPlaylistTimeWithoutOldSong =
//         substructSongTimeFromTotalPlaylistTime(
//             playlistOfSong.total_time,
//             oldSong.time
//         );
//     const toalPlaylistTime = addSongTimeToPlaylistTotalTime(
//         song.time,
//         String(totalPlaylistTimeWithoutOldSong)
//     );
//     playlistOfSong.total_time = toalPlaylistTime;

//     // update the song in his playlist
//     const songIndexToUpdate: number = playlistOfSong.songs?.findIndex(
//         (song: ISong) => song.id === songIDToUpdate
//     );
//     playlistOfSong.songs.splice(songIndexToUpdate, 1, song);
//     await playlistOfSong.save();
// }

// // delete song in each playlist in which the song in (Playlist.songs)
// export async function deletePlaylistOfSongByPlaylistID(
//     playlistID: number,
//     songIDToDelete: number,
//     song: ISong
// ) {
//     const playlistOfSong = await playlist_model.findById(playlistID);

//     // delete the song in his playlist
//     const songIndexToDelete: number = playlistOfSong.songs?.findIndex(
//         (song: ISong) => song.id === songIDToDelete
//     );
//     playlistOfSong.songs.splice(songIndexToDelete, 1);

//     // update playlist.number_of_songs
//     playlistOfSong.number_of_songs = playlistOfSong.songs.length;
//     if (playlistOfSong.number_of_songs === 0) {
//         // if no songs in the playlist - delete it
//         await deletePlaylistByID(playlistOfSong.id);
//         return;
//     }

//     // update playlist total time
//     const toalPlaylistTime = substructSongTimeFromTotalPlaylistTime(
//         playlistOfSong.total_time,
//         song.time
//     );
//     playlistOfSong.total_time = toalPlaylistTime;

//     await playlistOfSong.save();
// }

// **** Helper functions for playlist sercive ****

// Add Song Time To Total Playlist Time
// function addSongTimeToPlaylistTotalTime(
//     songTime: string,
//     playlistTime: string
// ): string {
//     const songTimeArr: string[] = songTime.split(":");
//     const [minutesOfSong, secondsOfSong] = songTimeArr;
//     const playlistTimeArr: string[] = playlistTime.split(":");
//     const [minutesOfPlaylist, secondsOfPlaylist] = playlistTimeArr;
//     let totalMinutes = Number(minutesOfSong) + Number(minutesOfPlaylist);
//     let totalSeconds = Number(secondsOfSong) + Number(secondsOfPlaylist);
//     if (totalSeconds >= 60) {
//         totalSeconds = Number(totalSeconds) - 60;
//         totalMinutes += 1;
//     }
//     const toalPlaylistTime = `${totalMinutes}:${totalSeconds}`;
//     return toalPlaylistTime;
// }

// // Substruct Song Time From Total Playlist Time
// function substructSongTimeFromTotalPlaylistTime(
//     playlistTime: string,
//     songTime: string
// ): string {
//     const songTimeArr: string[] = songTime.split(":");
//     const [minutesOfSong, secondsOfSong] = songTimeArr;
//     const playlistTimeArr: string[] = playlistTime.split(":");
//     const [minutesOfPlaylist, secondsOfPlaylist] = playlistTimeArr;
//     let totalMinutes = Number(minutesOfPlaylist) - Number(minutesOfSong);
//     let totalSeconds = Number(secondsOfPlaylist) - Number(secondsOfSong); //-10
//     if (totalSeconds < 0) {
//         totalSeconds = Number(totalSeconds) + 60;
//         totalMinutes -= 1;
//     }
//     const toalPlaylistTime = `${totalMinutes}:${totalSeconds}`;
//     return toalPlaylistTime;
// }
