import express, { Request, Response } from "express";
import { IPlaylist, IResponseMessage } from "../../../types/types.js";
import HttpException from "../../exceptions/http-exception.js";
import raw from "../../middleware/route.async.wrapper.js";
import { validateBeforeOperationMW } from "../../middleware/validations/schema.validation.js";
import {
    schemaForCreatePlaylist,
    schemaForUpdatePlaylist,
} from "./playlist.validation.js";
import {
    addSongToPlaylist,
    createNewPlaylist,
    deletePlaylistByID,
    getAllplaylists,
    getPlaylistByID,
    SONG_EXISTS,
    updatePlaylist,
} from "./playlist.service.js";
import { verifyAuth } from "../auth/auth.controller.js";

const router = express.Router();

// parse json req.body on post routes
router.use(express.json());

router
    .route("/")
    // Creates a new playlist (use validation MW before) and verify auth for user
    .post(raw(verifyAuth),
        raw(validateBeforeOperationMW(schemaForCreatePlaylist)),
        raw(async (req: Request, res: Response) => {
            console.log("req.body: ",req.body);
            
            const playlist = await createNewPlaylist(
                req.body as IPlaylist,
                Number(req.userID)
            );
            if (!playlist) {
                throw new HttpException(
                    400,
                    `No such user with id: ${req.userID}`
                );
            }
            const outputResponse: IResponseMessage = {
                status: 200,
                message: "Playlist created",
                data: playlist,
            };
            res.status(outputResponse.status).json(outputResponse);
        })
    )
    // Get all playlists
    .get(
        raw(async (req: Request, res: Response) => {
            const playlists = await getAllplaylists();
            const outputResponse: IResponseMessage = {
                status: 200,
                message: "All existing playlists",
                data: playlists,
            };
            res.status(outputResponse.status).json(outputResponse);
        })
    );

// Add a song to playlist (and update song.playlists field and playlist.total_time & playlist.number_of_songs fields)
// Only playlist creator can add songs to his playlist                                                                                                            localhost:3030/api/playlists?song_id=1234&playlist_id=5678
router.post(
    "/:playlist_id/song/:song_id",
    raw(verifyAuth),
    raw(async (req: Request, res: Response) => {
        const { playlist_id, song_id } = req.params;
        const playlist = await addSongToPlaylist(Number(playlist_id), Number(song_id),Number(req.userID));
        console.log("playlist: ",playlist);
        
        if (!playlist) {
            throw new HttpException(
                400,
                `No such playlist with id: ${playlist_id}`
            );
        }
        if (playlist === SONG_EXISTS) {
            throw new HttpException(
                400,
                `Song already exists in this playlist: ${playlist_id}`
            );
        } else {
            const outputResponse: IResponseMessage = {
                status: 200,
                message: `Song with id: ${song_id} added to playlist:`,
                data: playlist,
            };
            res.status(outputResponse.status).json(outputResponse);
        }
    })
);

router
    .route("/:id")
    // Gets a single playlist by ID
    .get(
        raw(async (req: Request, res: Response) => {
            const playlist = await getPlaylistByID(Number(req.params.id));
            if (!playlist) {
                throw new HttpException(
                    400,
                    `No such playlist with id: ${req.params.id}`
                );
            } else {
                const outputResponse: IResponseMessage = {
                    status: 200,
                    message: "Playlist found",
                    data: playlist,
                };
                res.status(outputResponse.status).json(outputResponse);
            }
        })
    )
    // Updates a single playlist by ID (use validation MW before) and verify auth for user
    // Only playlist creator can update playlist 
    .put(raw(verifyAuth),
        raw(validateBeforeOperationMW(schemaForUpdatePlaylist)),
        raw(async (req: Request, res: Response) => {
            const playlist = await updatePlaylist(
                Number(req.params.id),
                req.body as IPlaylist,
                Number(req.userID)
            );
            if (!playlist) {
                throw new HttpException(
                    400,
                    `No such playlist with id: ${req.params.id}`
                );
            } else {
                const outputResponse: IResponseMessage = {
                    status: 200,
                    message: "Playlist name updated",
                    data: playlist,
                };
                res.status(outputResponse.status).json(outputResponse);
            }
        })
    )
    // Deletes a playlist by ID
    .delete(raw(verifyAuth),
        raw(async (req: Request, res: Response) => {
            const playlist = await deletePlaylistByID(Number(req.params.id),Number(req.userID));
            if (!playlist) {
                throw new HttpException(
                    400,
                    `No such playlist with id: ${req.params.id}`
                );
            } else {
                const outputResponse: IResponseMessage = {
                    status: 200,
                    message: "Playlist deleted",
                    data: playlist,
                };
                res.status(outputResponse.status).json(outputResponse);
            }
        })
    );

export default router;
