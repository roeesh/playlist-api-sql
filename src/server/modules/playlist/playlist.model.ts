import mongoose from "mongoose";
import { SongSchema } from "../song/song.model.js";
const { Schema, model } = mongoose;

export const PlaylistSchema = new Schema(
    {
        name: { type: String, required: true },
        total_time: { type: String },
        songs: [SongSchema],
        number_of_songs: { type: Number },
        created_by: { type: Schema.Types.ObjectId, ref: "user" },
    },
    { timestamps: true }
);

export default model("playlist", PlaylistSchema);
