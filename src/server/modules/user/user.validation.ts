import Joi from "joi";

export const schemaForCreateUser = Joi.object({
    first_name: Joi.string().min(2).max(10).required(),

    last_name: Joi.string().min(3).max(10).required(),

    email: Joi.string()
        .email({ minDomainSegments: 2, tlds: { allow: ["com", "net"] } })
        .required(),

    // username: Joi.string().alphanum().min(3).max(30).required(),

    password: Joi.string()
        .pattern(new RegExp("^[a-zA-Z0-9]{3,30}$"))
        .required(),

    // phone: Joi.string().pattern(new RegExp("^[0-9]{10}$")).required(),
});

export const schemaForUpdateUser = Joi.object({
    first_name: Joi.string().min(2).max(10),

    last_name: Joi.string().min(3).max(10),

    // email: Joi.string().email({
    //     minDomainSegments: 2,
    //     tlds: { allow: ["com", "net"] },
    // }),

    // username: Joi.string().alphanum().min(3).max(30),

    // password: Joi.string().pattern(new RegExp("^[a-zA-Z0-9]{3,30}$")),

    // phone: Joi.string().pattern(new RegExp("^[0-9]{10}$")),
});
