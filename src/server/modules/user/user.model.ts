import mongoose from "mongoose";
const { Schema, model } = mongoose;

const UserSchema = new Schema(
    {
        first_name: { type: String, required: true },
        last_name: { type: String, required: true },
        email: { type: String, required: true },
        // username: { type : String, required : true },
        password: { type: String, required: true },
        // phone: { type : String, required : true },
        playlists: [{ type: Schema.Types.ObjectId, ref: "playlist" }],
        refresh_token: { type: String },
    },
    { timestamps: true }
);

export default model("user", UserSchema);
