import { connection as db} from "../../db/mysql.connection.js";
import bcrypt from "bcrypt";
import { IUser } from "../../../types/types.js";
import { RowDataPacket } from "mysql2";
// import user_model from "./user.model.js";
// import playlist_model from "../playlist/playlist.model.js";
// import { deletePlaylistByID } from "../playlist/playlist.service.js";


// Create new user
export async function createNewUser(reqBody: IUser): Promise<any> {
    reqBody.password = await bcrypt.hash(reqBody.password as string,10);
    // console.log({reqBody});
    const userInfo = reqBody;
    const sql = "INSERT INTO users SET ?";
    const [results] = await db.query(sql,userInfo);
    const json: any = results;
    
    const newUser = await getUserByID(Number(json.insertId));
    return newUser;
}

// Get all users
export async function getAllUsers(): Promise<any> {
    const [users] = await db.execute("SELECT * FROM users;");
    // console.log({users});
    return users;
}

// Get all users - with pagination
export async function getAllUsersWithPagination(
    page: string,
    limit: string
): Promise<any> {
    const [users] = await db.execute(`SELECT * FROM users LIMIT ${Number(page) * Number(limit)}, ${Number(limit)};`);
    return users;
}

// Get a single user
export async function getUserByID(idToRead: number): Promise<any> {
    const sql = `SELECT *
                 FROM users WHERE id= ${idToRead};`;
    const [user] = await db.execute(sql) as RowDataPacket[];
    if(user.length === 0){
        return undefined;
    }
    return user;
}

// Get a single user without password
export async function getUserWithoutPassword(idToRead: number): Promise<any> {
    const sql = `SELECT first_name, last_name, email 
                 FROM users WHERE id= ${idToRead};`;
    const [user] = await db.execute(sql) as RowDataPacket[];
    if(user.length === 0){
        return undefined;
    }
    return user;
}

// Get a single user by email
export async function getUserByEmail(
    email: string,
    // password: string
): Promise<any> {
    const sql = `SELECT * FROM users WHERE email= "${email}";`;
    const [user] = await db.execute(sql) as RowDataPacket[];
    if(user.length === 0){
        return undefined;
    }
    return user;
}

// Update a user using the id
export async function updateUser(
    idToUpdate: number,
    reqBody: IUser | any
): Promise<any> {
    const userInfo = reqBody;
    const sql = `UPDATE users SET ? WHERE id= ${idToUpdate};`;
    await db.query(sql,userInfo);

    const updatedUser = await getUserByID(idToUpdate);
    return updatedUser;
}

// Delete a user using the id
export async function deleteUserByID(idToDelete: number): Promise<any> {
    const sql = `DELETE FROM users WHERE id = ${idToDelete};`;
    const deletedUser = await getUserByID(idToDelete);
    await db.query(sql);
    
    return deletedUser;
}

// Get user by playlist id
// export async function getUserByPlaylistID(playlistID: number): Promise<any> {
//     const user = await playlist_model
//         .findById(playlistID)
//         .populate("created_by").select(`-_id 
//                                         first_name 
//                                         last_name 
//                                         email `);
//     return user;
// }

// // **** helper functions for playlist service ****

// // Delete playlist from User.playlists by user ID
// export async function deletePlaylistFromUser(
//     userID: number,
//     playlistIDToDelete: number
// ) {
//     const playlistCreator = await user_model.findById(userID); // update user.playlists
//     if (playlistCreator !== null) {
//         const playlistIndexToDelete: number =
//             playlistCreator.playlists.findIndex(
//                 (playlistID: number) => playlistID === playlistIDToDelete
//             );
//         playlistCreator.playlists.splice(playlistIndexToDelete, 1);
//         await playlistCreator.save();
//     }
// }
