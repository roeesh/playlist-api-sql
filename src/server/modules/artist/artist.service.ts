import { RowDataPacket } from "mysql2";
import { IArtist } from "../../../types/types.js";
// import artist_model from "./artist.model.js";
// import song_model from "../song/song.model.js";
// import { deleteSongByID } from "../song/song.service.js";
import { connection as db} from "../../db/mysql.connection.js";

// Create new artist
export async function createNewArtist(reqBody: IArtist): Promise<any> {
    const artistInfo = reqBody;
    const sql = "INSERT INTO artists SET ?";
    const [results] = await db.query(sql,artistInfo);
    const json: any = results;
    
    const newArtist = await getArtistByID(Number(json.insertId));
    return newArtist;
}

// Get all artists
export async function getAllArtists(): Promise<any> {
    const [artists] = await db.execute("SELECT * FROM artists;");
    // console.log({rows});
    return artists;
}

// Get all artists - with pagination
export async function getAllArtistsWithPagination(
    page: string,
    limit: string
): Promise<any> {
    const [rows] = await db.execute(`SELECT * FROM artists LIMIT ${Number(page) * Number(limit)}, ${Number(limit)};`);
    return rows;
}

// Get a single artist
export async function getArtistByID(idToRead: number): Promise<any> {
    const sql = `SELECT artists .*, GROUP_CONCAT(distinct songs.name) as songs
                FROM artists 
                LEFT JOIN songs
                    ON artists.id = songs.artist_id
                WHERE artists.id = ${idToRead};`;
    const [artist] = await db.execute(sql) as RowDataPacket[];
    if(artist.length === 0){
        return undefined;
    }
    return artist;
}

// Update a artist using the id
export async function updateArtist(
    idToUpdate: number,
    reqBody: IArtist
): Promise<any> {
    const artistInfo = reqBody;
    const sql = `UPDATE artists SET ? WHERE id= ${idToUpdate};`;
    await db.query(sql,artistInfo);

    const updatedArtist = await getArtistByID(idToUpdate);
    return updatedArtist;
}

// Delete a artist using the id
export async function deleteArtistByID(idToDelete: number): Promise<any> {
    const sql = `DELETE FROM artists WHERE id = ${idToDelete};`;
    const deletedArtist = await getArtistByID(idToDelete);
    await db.query(sql);
    return deletedArtist;
}

// Get artist by song id
export async function getArtistBySongID(songID: number): Promise<any> {
    const sql = `SELECT * FROM artists
                 INNER JOIN songs
                    ON artists.id = songs.artist_id
                 WHERE songs.id = ${songID};`;
    const [artist] = await db.execute(sql);
    return artist;
}

// // **** helper functions for song service ****

// // update song in Artist.songs by artist ID
// export async function updateArtistSongsByArtistID(
//     artistID: number,
//     songIDToUpdate: number,
//     song: ISong
// ) {
//     const artistOwner = await artist_model.findById(artistID); // update Artist.songs
//     const songIndexToUpdate: number = artistOwner.songs.findIndex(
//         (song: ISong) => song.id === songIDToUpdate
//     );
//     artistOwner.songs.splice(songIndexToUpdate, 1, song);
//     await artistOwner.save();
// }

// // Delete song from Artist.songs by artist ID
// export async function deleteArtistSongsByArtistID(
//     artistID: number,
//     songIDToDelete: number
// ) {
//     const artistOwner = await artist_model.findById(artistID); // update Artist.songs
//     const songIndexToDelete: number = artistOwner.songs.findIndex(
//         (song: ISong) => song.id === songIDToDelete
//     );
//     artistOwner.songs.splice(songIndexToDelete, 1);
//     await artistOwner.save();
// }
