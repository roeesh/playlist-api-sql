import mongoose from "mongoose";
import { SongSchema } from "../song/song.model.js";
const { Schema, model } = mongoose;

export const ArtistSchema = new Schema(
    {
        first_name: { type: String, required: true },
        last_name: { type: String, required: true },
        nickname: { type: String },
        email: { type: String },
        songs: [SongSchema],
        // songs: [ { type: Schema.Types.ObjectId, ref:"song"} ],
        // albums: [ { type: Schema.Types.ObjectId, ref:'album'} ],
        about: { type: String },
    },
    { timestamps: true }
);

export default model("artist", ArtistSchema);
