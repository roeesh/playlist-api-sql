/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error */
import raw from "../../middleware/route.async.wrapper.js";
import express, { Request, Response } from "express";
// import log from "@ajar/marker";
import { validateBeforeOperationMW } from "../../middleware/validations/schema.validation.js";
import {
    createNewArtist,
    getAllArtists,
    getArtistByID,
    updateArtist,
    deleteArtistByID,
    getAllArtistsWithPagination,
    getArtistBySongID,
} from "./artist.service.js";
import { IResponseMessage, IArtist } from "../../../types/types.js";
import HttpException from "../../exceptions/http-exception.js";
import { schemaForCreate, schemaForUpdate } from "./artist.validation.js";

const router = express.Router();

// parse json req.body on post routes
router.use(express.json());

router
    .route("/")
    // Create a new artist (use validation MW before)
    .post(
        raw(validateBeforeOperationMW(schemaForCreate)),
        raw(async (req: Request, res: Response) => {
            const artist = await createNewArtist(req.body as IArtist);
            const outputResponse: IResponseMessage = {
                status: 200,
                message: "Artist created",
                data: artist,
            };
            res.status(outputResponse.status).json(outputResponse);
        })
    )
    // Get all artists
    .get(
        raw(async (req: Request, res: Response) => {
            const artists = await getAllArtists();
            const outputResponse: IResponseMessage = {
                status: 200,
                message: "All existing artists",
                data: artists,
            };
            res.status(outputResponse.status).json(outputResponse);
        })
    );

router
    .route("/:id")
    // Gets a single user by ID
    .get(
        raw(async (req: Request, res: Response) => {
            const artist = await getArtistByID(Number(req.params.id));
            if (!artist) {
                throw new HttpException(
                    400,
                    `No such artist with id: ${req.params.id}`
                );
            } else {
                const outputResponse: IResponseMessage = {
                    status: 200,
                    message: "Artist found",
                    data: artist,
                };
                res.status(outputResponse.status).json(outputResponse);
            }
        })
    )
    // Updates a single user by ID (use validation MW before)
    .put(
        raw(validateBeforeOperationMW(schemaForUpdate)),
        raw(async (req: Request, res: Response) => {
            const artist = await updateArtist(
                Number(req.params.id),
                req.body as IArtist
            );
            if (!artist) {
                throw new HttpException(
                    400,
                    `No such artist with id: ${req.params.id}`
                );
            } else {
                const outputResponse: IResponseMessage = {
                    status: 200,
                    message: "artist updated",
                    data: artist,
                };
                res.status(outputResponse.status).json(outputResponse);
            }
        })
    )
    // Deletes a artist by ID
    .delete(
        raw(async (req: Request, res: Response) => {
            const artist = await deleteArtistByID(Number(req.params.id));
            if (!artist) {
                throw new HttpException(
                    400,
                    `No such artist with id: ${req.params.id}`
                );
            } else {
                const outputResponse: IResponseMessage = {
                    status: 200,
                    message: "artist deleted",
                    data: artist,
                };
                res.status(outputResponse.status).json(outputResponse);
            }
        })
    );

// Get all artists - pagination
router.get(
    "/pagination/:page/:limit",
    raw(async (req: Request, res: Response) => {
        const artists = await getAllArtistsWithPagination(
            req.params.page,
            req.params.limit
        );
        const outputResponse: IResponseMessage = {
            status: 200,
            message: `All existing artists within the page ${req.params.page} and limit ${req.params.limit}`,
            data: artists,
        };
        res.status(outputResponse.status).json(outputResponse);
    })
);

// Get artist by song id
router.get(
    "/song/:id",
    raw(async (req, res) => {
        const artist = await getArtistBySongID(Number(req.params.id));
        if (!artist) {
            throw new HttpException(
                400,
                `No such song with id: ${req.params.id}`
            );
        } else {
            const outputResponse: IResponseMessage = {
                status: 200,
                message: `The artist of the song: ${req.params.id}`,
                data: artist,
            };
            res.status(outputResponse.status).json(outputResponse);
        }
    })
);

export default router;
