import Joi from "joi";

export const schemaForCreate = Joi.object({
    first_name: Joi.string().min(2).max(10).required(),

    last_name: Joi.string().min(3).max(10).required(),

    nickname: Joi.string().min(2).max(20),

    email: Joi.string().email({
        minDomainSegments: 2,
        tlds: { allow: ["com", "net"] },
    }),

    about: Joi.string().max(100),
});

export const schemaForUpdate = Joi.object({
    first_name: Joi.string().min(2).max(10),

    last_name: Joi.string().min(3).max(10),

    nickname: Joi.string().min(2).max(10),

    email: Joi.string().email({
        minDomainSegments: 2,
        tlds: { allow: ["com", "net"] },
    }),

    about: Joi.string().max(100),
});
