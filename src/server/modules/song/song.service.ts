// import pkg from "bluebird";
import { RowDataPacket } from "mysql2";
import { ISong } from "../../../types/types.js";
// import artist_model from "../artist/artist.model.js";
// import {
//     deleteArtistSongsByArtistID,
//     updateArtistSongsByArtistID,
// } from "../artist/artist.service.js";
// import {
//     // deletePlaylistOfSongByPlaylistID,
//     // updatePlaylistOfSongByPlaylistID,
// } from "../playlist/playlist.service.js";
// import song_model from "./song.model.js";
import { connection as db} from "../../db/mysql.connection.js";


// const { Promise } = pkg;
export const NO_OWNER = "no such owner";

// Create new song (and insert it to its owner songs list)
export async function createNewSong(reqBody: ISong): Promise<any> {
    const songInfo = reqBody;
    const sql = "INSERT INTO songs SET ?";
    const [results] = await db.query(sql,songInfo);
    const json: any = results;
    
    const newSong = await getSongByID(Number(json.insertId));
    return newSong;
}

// Get all songs
export async function getAllASongs(): Promise<any> {
    const [songs] = await db.execute("SELECT * FROM songs;");
    // console.log({songs});
    return songs;
}

// Get all songs of an artist by his ID
export async function getArtistsSongsByID(artistID: number): Promise<any> {
    const sql = `SELECT * FROM songs
                 INNER JOIN artists
                    ON songs.artist_id = artists.id
                 WHERE artists.id = ${artistID};`;
    const [songsOfArtist] = await db.execute(sql);
    return songsOfArtist;
}

// Get a single song
export async function getSongByID(idToRead: number): Promise<any> {
    const sql = `SELECT * FROM songs WHERE id= ${idToRead};`;
    const [song] = await db.execute(sql) as RowDataPacket[];
    if(song.length === 0){
        return undefined;
    }
    return song;
}

// Update a song using the id
export async function updateSong(
    idToUpdate: number,
    reqBody: ISong
): Promise<any> {
    const songInfo = reqBody;
    const sql = `UPDATE songs SET ? WHERE id= ${idToUpdate};`;
    await db.query(sql,songInfo);

    const updatedSong = await getSongByID(idToUpdate);
    return updatedSong;
    // update song in Artist.songs
    // await updateArtistSongsByArtistID(song.owner, idToUpdate, song);

    // update song in each playlist in which the song in (Playlist.songs)
    // Promise.each(song.playlists, async (playlistID: number) => {
    //     await updatePlaylistOfSongByPlaylistID(
    //         playlistID,
    //         idToUpdate,
    //         song,
    //         oldSong
    //     );
    // });
    // return song;
}

// Delete a song using the id
export async function deleteSongByID(idToDelete: number): Promise<any> {
    const sql = `DELETE FROM songs WHERE id = ${idToDelete};`;
    const deletedSong = await getSongByID(idToDelete);
    await db.query(sql);
    
    return deletedSong;
}

// // **** helper functions for playlist service ****

// // delete playlist from each song in which the playlist in (Song.Playlists)
// export async function deletePlaylistFromSong(
//     playlistIDToDelete: number,
//     song: ISong
// ) {
//     const songFromPlaylist = await song_model.findById(song.id);
//     if (songFromPlaylist !== null) {
//         const playlistIndexToDelete: number =
//             songFromPlaylist.playlists?.findIndex(
//                 (playlistID: number) => playlistID === playlistIDToDelete
//             );
//         songFromPlaylist.playlists.splice(playlistIndexToDelete, 1);
//         await songFromPlaylist.save();
//     }
// }
