import Joi from "joi";

export const schemaForCreateSong = Joi.object({
    name: Joi.string().min(2).max(20).required(),

    time: Joi.string().min(3).max(10).required(),

    artist_id: Joi.string().max(30).required(),

    // label: Joi.string().max(50),

    lyrics: Joi.string().max(1000),
});

export const schemaForUpdateSong = Joi.object({
    name: Joi.string().min(2).max(20),

    time: Joi.string().min(3).max(10),

    // label: Joi.string().max(50),

    lyrics: Joi.string().max(1000),
});
