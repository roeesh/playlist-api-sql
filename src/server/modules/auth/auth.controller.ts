import bcrypt from "bcrypt";
import express, { NextFunction, Request, Response } from "express";
import jwt from "jsonwebtoken";
import ms from "ms";
import cookieParser from "cookie-parser";
import log from "@ajar/marker";
import { createNewUser, getUserByEmail, getUserByID, getUserWithoutPassword, updateUser } from "../user/user.service.js";
import { IResponseMessage, IUser } from "../../../types/types.js";
import { validateBeforeOperationMW } from "../../middleware/validations/schema.validation.js";
import raw from "../../middleware/route.async.wrapper.js";
import { schemaForCreateUser } from "../user/user.validation.js";
import unauthorizedException from "../../exceptions/unauthorized-exception.js";
import HttpException from "../../exceptions/http-exception.js";

const { APP_SECRET, ACCESS_TOKEN_EXPIRATION, REFRESH_TOKEN_EXPIRATION } =
    process.env;

const router = express.Router();
router.use(cookieParser());
router.use(express.json());

// verify middleware for protected routes
export const verifyAuth = async (
    req: Request,
    res: Response,
    next: NextFunction
) => {
        const access_token = req.headers["x-access-token"];

        if (!access_token){
            throw new unauthorizedException("Unauthorized - No token provided.");
        }
           
        // verifies secret and checks exp
        const decoded = await jwt.verify(
            access_token as string,
            APP_SECRET as string
        );

        // if everything is good, save to request for use in other routes
        req.userID = (decoded as jwt.JwtPayload).id;
        req.userRoles = (decoded as jwt.JwtPayload).roles;
        // console.log("req.userID: ",req.userID);
        // console.log("req.userRoles: ",req.userRoles);

        next();
};

// protected route to get the login user data
router.get("/whoami", raw(verifyAuth), raw(async (req: Request, res: Response) => {
    log.v("req.user_id:", req.userID);
    const user = await getUserWithoutPassword(Number(req.userID));
    // const payload: IUser = { ...user };
    // delete payload.password;
    const outputResponse: IResponseMessage = {
        status: 200,
        message: "OK",
        data: user,
    };
    res.status(outputResponse.status).json(outputResponse);
}));

// register router (create new user)
router.post("/register", 
    raw(validateBeforeOperationMW(schemaForCreateUser)), 
    raw(async (req: Request,res: Response)=>{
        const {email} = req.body;
        const userToCheck = await getUserByEmail(email);
        if(userToCheck) {
            throw new HttpException(409,"Email already exists. try another one.");
        }

        const [user] = await createNewUser(req.body as IUser);
        const userWithoutPassword = await getUserWithoutPassword(user.id);

        const outputResponse: IResponseMessage = {
            status: 200,
            message: "User created",
            data: userWithoutPassword,
        };
        res.status(outputResponse.status).json(outputResponse);
    })
);

// login router
router.post("/login", raw(async (req: Request,res: Response)=> {
    // log.obj(req.body,"body");
    const {email,password} = req.body;
    const [user] = await getUserByEmail(email);
    if(!user || user === null) {
        throw new unauthorizedException("Unauthorized - wrong email or password.");
    }
    if(email === user.email && await bcrypt.compare(password,user.password)){
        
        // create access token
        const access_token = jwt.sign({ id : user.id, roles: user.roles }, APP_SECRET as string, {
            expiresIn: ACCESS_TOKEN_EXPIRATION // expires in 1 minute for debugging...
        });

        // create refresh token
        const refresh_token = jwt.sign({ id : user.id, roles: user.roles }, APP_SECRET as string, {
            expiresIn: REFRESH_TOKEN_EXPIRATION // expires in 60 days... long-term... 
          });
        
        // save refresh token in database
        const payloadToUpdate = {
            refresh_token: refresh_token
        };
        await updateUser(Number(user.id),payloadToUpdate);
        // user.refresh_token = refresh_token;
        // user.save();

        // save refresh token in http only cookie
        res.cookie("refresh_token",refresh_token, {
            maxAge: ms("60d"), //60 days
            httpOnly: true
        });

        // const userWithoutPassword = await getUserWithoutPassword(user.id);
        const userToReturn = await getUserByID(user.id);
        // const payload: IUser = {...user};
        // delete payload.password;

        const outputResponse: IResponseMessage = {
            status: 200,
            message: "You are authenticated",
            data: {
                user: userToReturn,
                access_token: access_token
            }
        };
        res.status(outputResponse.status).json(outputResponse);
    }else{
        throw new unauthorizedException("Unauthorized - wrong email or password.");
    }
}));

// get-access-token router to request to generate access token
router.get("/get-access-token", raw(async (req: Request, res: Response) => {
    //get refresh_token from client - req.cookies
    const { refresh_token } = req.cookies;

    console.log({ refresh_token });

    if (!refresh_token){
        throw new unauthorizedException("Unauthorized - No refresh_token provided.");
    }

        // verifies secret and checks expiration
        const decoded = await jwt.verify(refresh_token, APP_SECRET as string);
        console.log({ decoded });

        //check user refresh token in DB
        const { id, roles } = decoded as jwt.JwtPayload;
        const [user] = await getUserByID(id);

        if(user.refresh_token != refresh_token){
            throw new unauthorizedException("Unauthorized - refresh_token is not valid. Please login.");
        }

        const access_token = jwt.sign({ id, roles }, APP_SECRET as string, {
            expiresIn: ACCESS_TOKEN_EXPIRATION, //expires in 1 minute
        });
        const outputResponse: IResponseMessage = {
            status: 200,
            message: "Access_token created.",
            data: access_token,
        };
        res.status(outputResponse.status).json(outputResponse);
}));

router.get("/logout",raw(verifyAuth), raw(async (req, res) => {
    res.clearCookie("refresh_token");

    const payloadToUpdate = {
        refresh_token: ""
    };
    const [user] = await updateUser(Number(req.userID),payloadToUpdate);
    const userWithoutPassword = await getUserWithoutPassword(user.id);

    // const payload: IUser = { ...user };
    // delete payload.password;

    const outputResponse: IResponseMessage = {
        status: 200,
        message: "You are logged out.",
        data: userWithoutPassword,
    };
    res.status(outputResponse.status).json(outputResponse);
}));

export default router;
