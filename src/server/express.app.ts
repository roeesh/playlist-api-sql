import express from "express";
import morgan from "morgan";
import log from "@ajar/marker";
import cors from "cors";

// import { connect_db } from "./db/mongoose.connection.js";
import { connect as connect_sqlDb} from "./db/mysql.connection.js";
import artist_controller from "./modules/artist/artist.controller.js";
import song_controller from "./modules/song/song.controller.js";
import playlist_controller from "./modules/playlist/playlist.controller.js";
import user_controller from "./modules/user/user.controller.js";
import auth_controller from "./modules/auth/auth.controller.js";

import {
    errorLogger,
    not_found,
    responseWithError,
    urlNotFoundHandler,
} from "./middleware/errors.handler.js";
import { generateRequestID } from "./middleware/requestId.js";
import { appendLogToFile } from "./middleware/http.log.js";
import { appendToErrorLogger } from "./middleware/error.log.js";
import { saverTask } from "./scheduler/backup.schedule.js";

const { PORT, HOST = "localhost" } = process.env;
const { HTTP_LOG_FILE_PATH, ERRORS_LOG_FILE_PATH } = process.env;
const LEVEL = "app level";

class ExpressApp {
    app = express();

    private setMiddlewares() {
        this.app.use(generateRequestID);
        this.app.use(cors());
        this.app.use(morgan("dev"));
        this.app.use(appendLogToFile(HTTP_LOG_FILE_PATH as string, LEVEL));
    }

    private setRoutings() {
        this.app.use("/api/artists", artist_controller);
        this.app.use("/api/songs", song_controller);
        this.app.use("/api/playlists", playlist_controller);
        this.app.use("/api/users", user_controller);
        this.app.use("/api", auth_controller);
    }

    private setErrorHandlers() {
        this.app.use(urlNotFoundHandler);
        this.app.use(errorLogger);
        this.app.use(appendToErrorLogger(ERRORS_LOG_FILE_PATH as string));
        this.app.use(responseWithError);
        // this.app.use(error_handler);
        // this.app.use(error_handler2);
    }

    private setDefault() {
        this.app.use("*", not_found);
    }

    async start() {
        this.setMiddlewares();
        this.setRoutings();
        this.setErrorHandlers();
        this.setDefault();
        //connect to mongo db
        // await connect_db(DB_URI as string);

        //connect to mySql
        await connect_sqlDb();

        await this.app.listen(Number(PORT), HOST as string);
        log.magenta("api is live on", ` ✨ ⚡  http://${HOST}:${PORT} ✨ ⚡`);
        // save db as file by saver server
        saverTask.start();
    }
}

const instance = new ExpressApp();
export default instance;
