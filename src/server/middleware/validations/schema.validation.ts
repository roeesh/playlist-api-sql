import { NextFunction, Request, Response } from "express";
import Joi from "joi";

// Middleware to validate user before operation
export const validateBeforeOperationMW =
    (schemaToValidate: Joi.ObjectSchema<any>) =>
    async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        await schemaToValidate.validateAsync(req.body);
        next();
    };
