import { NextFunction, Request, Response } from "express";
import { user_roles } from "../../../types/types.js";
import unauthorizedException from "../../exceptions/unauthorized-exception.js";

// verify middleware to check if the ligin user is a basic user
export const isAdminVerifier = async (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    const userRolesArr = req.userRoles.split(",");
    if(userRolesArr.includes(user_roles.Simple) === false){
        throw new unauthorizedException("Unauthorized - please login.");
    }
    next();
};