import { NextFunction, Request, Response } from "express";
import unauthorizedException from "../../exceptions/unauthorized-exception.js";

// verify middleware to check if the ligin user is admin
export const verifyPermissionAs = (role: string) => async (req: Request, res: Response, next: NextFunction) => {
    const userRolesArr = req.userRoles.split(",");
    if(userRolesArr.includes(role) === false){
        throw new unauthorizedException(`Unauthorized - no ${role} permission`);
    }
    next();
};