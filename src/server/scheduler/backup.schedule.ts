import cron from "node-cron";
import { saveDbToFile } from "../db/mysql.backup.js";

const { BACKUP_SERVER_PORT } = process.env;

// every 2 minute
export const saverTask = cron.schedule("*/2 * * * *",async () => {
    try{
        await saveDbToFile(BACKUP_SERVER_PORT as string);
    }catch(err){
        console.log(err);
    }
},{
    scheduled: false
});

