import fetch from "node-fetch";
import { exec } from "child_process";
import log from "@ajar/marker";
import fs from "fs/promises";

const { HOST = "localhost" } = process.env;

export const saveDbToFile = (portOfSaver: string) => {
    fs.readFile("./config.json").then(async (data)=>{
        const dbObject = JSON.parse(data.toString());
        for (const db in dbObject) {
            log.blue("-> Sent file:", db);
            let dumpFileName = `${db}DB`;
            
            dumpFileName += `${Date.now()}`;
            const task = exec(
                `docker exec mysql-db /usr/bin/mysqldump -u root --password=qwerty ${db}`
            );
            try{
                const res = await fetch(`http://${HOST}:${portOfSaver}/saver`, {
                    method: "post",
                    body: task.stdout,
                    headers: {"db-name": dumpFileName}
                });
                const data = await res.json();
                console.log({data});
            } catch(err){
                console.log(err);
            }
        }
    });
};